package opensourceCMS.com.login;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import opensourceCMS.com.genericlib.BaseTest;
import opensourceCMS.com.genericlib.SeleniumUtil;
import opensourceCMS.com.pageobjects.LoginPage;

public class LoginTest_with_POM extends BaseTest{

	
	//Create object for LoginPage Class
	LoginPage login ;
	
	@Test
	public void LoginTest01() {
		//Create object for Loginpage class using PageFactory
		login = PageFactory.initElements(driver, LoginPage.class);
		
		login.getUserNameTxtbox().sendKeys("opensourcecms");
		System.out.println("entered username : opensourcecms");
		
		login.getPasswordTxtBox().sendKeys("opensourcecms");
		
		login.getLoginButton().click();
		
		//Implicit Wait
		SeleniumUtil.waitForPageToLoad();
		
		
	}
	
	@Test
	public void LoginTest02() {
		
	}
	
}
