package opensourceCMS.com.users;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import opensourceCMS.com.genericlib.BaseTest;
import opensourceCMS.com.genericlib.DataParser;
import opensourceCMS.com.pageobjects.UserRegisterPage;

public class RegisterNewUserDDT extends BaseTest{

	//Page object class
	UserRegisterPage page;
	
	String testDataFile = "src\\test\\resources\\datafiles\\smoketest\\UserRegistration.xls";
	
	
	@DataProvider(name="createNewAccount")
	public Object[] [] getData(){
		
		//Sending only file name and sheet name
		//String[] [] res = new DataParser().getTestDataForDDT("UserRegistration", "Account_Creation");
		
		//Sending test data file as relative path
		String[] [] res = new DataParser().runTestWithMultipleSetsOfData(testDataFile, "Account_Creation");
		
		return res;
	}
	
	
	@Test(dataProvider="createNewAccount", groups= {"smoke"}, description="executing createAccount test case with multiple sets of Data")
	public void createNewAccount(String firstName, String lastName, String email, String pwd) {
		
		page = new UserRegisterPage(driver);
		
		page.createNewAccount(firstName, lastName, email, pwd);
		
	}
	
	
	
	
}
