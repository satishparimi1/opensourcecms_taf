package opensourceCMS.com.users;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import jxl.read.biff.BiffException;
import opensourceCMS.com.genericlib.BaseTest;
import opensourceCMS.com.genericlib.DataParser;
import opensourceCMS.com.pageobjects.UserRegisterPage;

public class RegisterNewUsers extends BaseTest{
	
	public UserRegisterPage page;
	
	//Test Data File
	String filePath = "src\\test\\resources\\datafiles\\smoketest\\UserRegistration.xls";
	
	
	@Test(groups= {"smoke", "regression"})
	public void registerNewUser() throws InterruptedException {
		
			//page = PageFactory.initElements(driver, UserRegisterPage.class);
			
			//page.getRegisterTab().click();
			//Thread.sleep(5000);
		
		page = new UserRegisterPage(driver);
			
		try {
			List<String> res = new DataParser().getTestcaseData(filePath, "RegisterUsers", "TC1125");
			
			page.createNewAccount(res.get(0), res.get(1), res.get(2), res.get(3));
			
			Thread.sleep(10000);
			
		} catch (BiffException | IOException e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
			
			
	}

	
	
}
