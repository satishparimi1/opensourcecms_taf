package opensourceCMS.com.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPage {

	
	@FindBy(how=How.ID_OR_NAME, using="user_login")
	private WebElement username;
	
	
	//We will use encapsulation concept from Java
	//Def: giving access to the private members outside of the class through public methods
	
	//Getter
	public WebElement getUserNameTxtbox() {
		
		return username;
	}

	
	
	@FindBy(how = How.ID_OR_NAME, using = "user_pass")
	private WebElement password;
	
	public WebElement getPasswordTxtBox() {
		
		return password;
	}
	
	
	
	@FindBy(how = How.ID_OR_NAME, using = "wp-submit")
	private WebElement login;
	
	public WebElement getLoginButton() {
		return login;
	}
	
	
}
