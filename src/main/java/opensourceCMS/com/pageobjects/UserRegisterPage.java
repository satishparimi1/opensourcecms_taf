package opensourceCMS.com.pageobjects;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import jxl.read.biff.BiffException;
import opensourceCMS.com.genericlib.BaseTest;
import opensourceCMS.com.genericlib.DataParser;

public class UserRegisterPage extends BaseTest{
	
	
	/**
	 * Approach - 2 : to create Re-Usable components
	 */
	WebDriver driver;
	
	public UserRegisterPage(WebDriver driver){

        this.driver = driver;

        PageFactory.initElements(driver, this);

    }
	
	
	@FindBy(how =How.XPATH, using = "//a[contains(@href,'/register')]")
	private static WebElement register;
	
	
	@FindBy(how =How.XPATH, using ="//input[@id='gender-male']")
	private static WebElement gender;
	
	@FindBy(how =How.XPATH, using ="//input[@id='FirstName']")
	private static WebElement firstName;
	
	@FindBy(how =How.XPATH, using ="//input[@id='LastName']")
	private static WebElement lastName;
	
	@FindBy(how =How.XPATH, using ="//input[@id='Email']")
	private static WebElement email;
	
	@FindBy(how =How.XPATH, using ="//input[@id='Password']")
	private static WebElement password;
	
	@FindBy(how =How.XPATH, using ="//input[@id='ConfirmPassword']")
	private static WebElement confirmPassword;
	
	
	public WebElement getRegisterTab() {
		return register;
	}
	
	public WebElement getGender() {
		return gender;
	}
	public WebElement getFirstName() {
		return firstName;
	}
	public WebElement getLastName() {
		return lastName;
	}
	public WebElement getEmail() {
		return email;
	}
	
	public WebElement getPassword() {
		return password;
	}
	
	public WebElement getConfirmpassword() {
		return confirmPassword;
	}
	
	
	/***********************************************************/
	public void clickOnRegisterTab() {
		
		register.click();
	}
	
	
	
	public void createNewAccount(String fn, String ln, String e, String p) {
		
		//Click on Register tab
		register.click();
		logger.info("Clicked on Registration Tab");
		
		//Select the gender
		gender.click();
		logger.info("Clicked on Gender radio button");
		
		//Enter the firstName
		firstName.sendKeys(fn);
		logger.info("Entered FirstName : "+fn);
		
		//Enter the last name
		lastName.sendKeys(ln);
		logger.info("Entered LastName : "+ln);
		
		//Enter email
		email.sendKeys(e);
		logger.info("Entered email : "+e);
		
		//Enter password
		password.sendKeys(p);
		logger.info("Entered pwd : "+p);
		
		//Enter confirm password
		confirmPassword.sendKeys(p);
		logger.info("Entered pwd : "+p);
	}
	
	
	
	
	
	
	
}
