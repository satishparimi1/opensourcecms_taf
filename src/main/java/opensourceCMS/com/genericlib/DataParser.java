package opensourceCMS.com.genericlib;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class DataParser extends BaseTest {

	/**
	 * @author sparimi
	 * @Purpose : getExcelData() for Data Driven Testing of test case
	 * @param fileName
	 * @param sheetName
	 * @return String[] Array of Data
	 * 
	 */
	
	
	//Use this method to run your test case with multiple sets of data
	public String[][] getTestDataForDDT(String fileName, String sheetName) {
		String[][] arrayExcelData = null;

		logger.info("File Name is : " + fileName);
		String filepath = new String(
		System.getProperty("user.dir") + "\\src\\test\\resources\\datafiles\\smoketest\\" + fileName + ".xls");
		logger.info("File Path is : " + filepath);

		try {
			FileInputStream fs = new FileInputStream(filepath);
			Workbook wb = Workbook.getWorkbook(fs);
			Sheet sh = wb.getSheet(sheetName);

			int totalNoOfCols = sh.getColumns();
			int totalNoOfRows = sh.getRows();

			arrayExcelData = new String[totalNoOfRows - 1][totalNoOfCols];

			for (int i = 1; i < totalNoOfRows; i++) {

				for (int j = 0; j < totalNoOfCols; j++) {
					arrayExcelData[i - 1][j] = sh.getCell(j, i).getContents();
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			e.printStackTrace();
		} catch (BiffException e) {
			e.printStackTrace();
		}
		return arrayExcelData;

	}

	
	
	
	
	
	/**
	 * @author sparimi
	 * @Purpose : runTestWithMultipleSetOfData() for Data Driven Testing of test case
	 * @param relative filePath from test case: Send relative path of the file and construct the complete filePath using "File" class
	 * @param sheetName
	 * @return String[] Array of Data
	 * 
	 */
	
	
	//
	public String[][] runTestWithMultipleSetsOfData(String filePath, String sheetName) {
		String[][] arrayExcelData = null;
		
		//Construct complete file Path
		File file = new File(filePath);
		
		String completeFilePath = null;
		
		try {
			 completeFilePath = file.getCanonicalPath();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		logger.info("Complete test data file path is : " + completeFilePath);

		try {
			FileInputStream fs = new FileInputStream(completeFilePath);
			Workbook wb = Workbook.getWorkbook(fs);
			Sheet sh = wb.getSheet(sheetName);

			int totalNoOfCols = sh.getColumns();
			int totalNoOfRows = sh.getRows();

			arrayExcelData = new String[totalNoOfRows - 1][totalNoOfCols];

			for (int i = 1; i < totalNoOfRows; i++) {

				for (int j = 0; j < totalNoOfCols; j++) {
					arrayExcelData[i - 1][j] = sh.getCell(j, i).getContents();
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			e.printStackTrace();
		} catch (BiffException e) {
			e.printStackTrace();
		}
		return arrayExcelData;

	}

	
	
	
	
	
	
	/**
	 * @Purpose: Returns List<String> output havign all columns of Data based on
	 *           Test Case Number
	 * @param fileName
	 * @param sheetName
	 * @param testcaename
	 * @return
	 * @throws BiffException
	 * @throws IOException
	 */
	// Get Data method - to run your test case with signle set of data
	public List<String> getTestcaseData(String fileName, String sheetName, String testcaename)
			throws BiffException, IOException {
		String col_01_value;
		String col1;

		List<String> list = new ArrayList<String>();

		String filePath = new String(System.getProperty("user.dir") + "\\src\\test\\resources\\datafiles\\smoketest\\" + fileName + ".xls");
		logger.info("File Path is : " + filePath);
		

		FileInputStream fs = new FileInputStream(filePath);
		Workbook wb = Workbook.getWorkbook(fs);

		// TO get the access to the sheet
		Sheet sh = wb.getSheet(sheetName);

		// To get the number of rows present in sheet
		int totalNoOfRows = sh.getRows();
		logger.info("Total Rows : " + totalNoOfRows);
		
		// To get the number of columns present in sheet
		int totalNoOfCols = sh.getColumns();
		logger.info("Total columns : " + totalNoOfCols);

		for (int row = 0; row < totalNoOfRows; row++) {
			for (int col = 0; col < totalNoOfCols; col++) {
				if (col == 0) {
					col_01_value = sh.getCell(col, row).getContents();
					if (!col_01_value.equalsIgnoreCase(testcaename)) {
						break;
					}

					col = col + 1;
				}

				
				col1 = sh.getCell(col, row).getContents();

				list.add(col1);
				

			}

		}

		/*
		 * for(Object o:list) { System.out.println(o); }
		 */

		return list;
	}
	
	
	
	
	

}
