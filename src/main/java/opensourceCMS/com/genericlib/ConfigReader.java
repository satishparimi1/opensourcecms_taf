package opensourceCMS.com.genericlib;

import java.io.FileReader;
import java.util.Properties;

public class ConfigReader {

	
	//Default Environment configurations
	public static String fileName = System.getProperty("user.dir")+"\\Config\\Sandbox.properties";
	
	public static Properties props ;
	
	public ConfigReader() {
		
		try {
			FileReader reader=new FileReader(fileName);  
			
			props = new Properties();
			
			props.load(reader);
			
		}catch(Exception e) {
			
			System.out.println("File not loaded  !!");
			e.printStackTrace();
		}
	}
	
	
	public String getURL() {
		
		String url = props.getProperty("URL");
		
		return url;
	}
	
	public String getUN() {
		
		String un = props.getProperty("UN");
		
		return un;
	}
	
	public String getPwd() {
		
		String pwd = props.getProperty("PWD");
		return pwd;
	}
	
}
