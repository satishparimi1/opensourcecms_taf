package opensourceCMS.com.genericlib;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BaseTest {
	
		// Extent Reports object declaration
		public static ExtentHtmlReporter htmlReporter;
		public static ExtentReports extent;
		public static ExtentTest test;
		
		
	
	public BaseTest() {
		
		System.out.println("BaseTest() constructor executed....");
		
		//Here we will load logger object
		setLogger();
	}
	
	
	
	 //Logger Object
		protected Logger logger = null;
		
		//Set Logger method
		public void setLogger() {
			
			logger = TestLogger.init(this.getClass().getSimpleName());
		}
		
		//Get Logger Method
		public Logger getLogger() {
			
			return TestLogger.getLogger();
		}
	
	
	
	public static WebDriver driver ;
	
	public static ConfigReader config;

	@BeforeMethod
	public void beforeMethod() {
		
		//launch browser code
		System.out.println("Beforemethod() from base test..started !!");
		launchBrowser("Chrome");
		
	}
	
	
	@AfterMethod
	public void afterMethod() {
		
		//close browser code
		System.out.println("afterMethod() from base test");
		
		//Close the browser
		driver.close();
	}
	
		
	
	
	
	//launch browser
	public void launchBrowser(String browserName) {
		
		
		//Open New Browser
		if(browserName.equalsIgnoreCase("Chrome")) {
			
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\Lib\\chromedriver.exe");
			driver = new ChromeDriver();
			
		}else if(browserName.equalsIgnoreCase("Firefox")) {
			
			System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"\\Lib\\geckodriver.exe");
			driver = new FirefoxDriver();
			
		}else if(browserName.equalsIgnoreCase("IE")) {
			
			System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"\\Lib\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			
		}else if(browserName.equalsIgnoreCase("Edge")) {
			
			System.setProperty("webdriver.edge.driver", System.getProperty("user.dir")+"\\Lib\\MicrosoftWebDriver.exe");
			driver = new EdgeDriver();
			
		}else {
			System.out.println("****************** No browser is launched....!!!, Pls check browser name **************");
		}
		
		//maximize the browser
		driver.manage().window().maximize();
		
		navigateToApplication();
		
		
		
	}
	
	
	public void navigateToApplication() {
		
		config = new ConfigReader();
		
		String url =config.getURL();
	
		driver.get(url);
		
		
	}
	
	
	
	public String captureScreenshot(WebDriver driver, String screenshotName) {

		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String dest = System.getProperty("user.dir") + "\\ExtentReports\\Screenshots\\" + screenshotName + ".png";
		File destination = new File(dest);
		try {
			FileUtils.copyFile(source, destination);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return dest;
	}
	
	
}
