package opensourceCMS.com.genericlib;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader{

	/*public static String filePath ;
	public static String sheetName;*/
	 
	

	/*public static List<Comparable> readXLSXFile(String fileName, String sheetName) {

		List<Comparable> data = new ArrayList<Comparable>();

		FileInputStream XlsxFileToRead = null;
		XSSFWorkbook workbook = null;
		try {

			String filePath = new String(System.getProperty("user.dir") + "\\src\\test\\resources\\datafiles\\smoketest\\" + fileName + ".xlsx");
			//logger.info("File Path is : " + filePath);
			System.out.println(filePath);
			XlsxFileToRead = new FileInputStream(filePath);

			// Getting the workbook instance for xlsx file
			workbook = new XSSFWorkbook(XlsxFileToRead);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// getting the first sheet from the workbook using sheet name.
		// We can also pass the index of the sheet which starts from '0'.
		XSSFSheet sheet = workbook.getSheet(sheetName);
		XSSFRow row;
		XSSFCell cell;

		// Iterating all the rows in the sheet
		Iterator<?> rows = sheet.rowIterator();

		while (rows.hasNext()) {
			row = (XSSFRow) rows.next();

			// Iterating all the cells of the current row
			Iterator<?> cells = row.cellIterator();

			while (cells.hasNext()) {
				cell = (XSSFCell) cells.next();

				if (cell.getCellTypeEnum() == CellType.STRING) {
					//System.out.print(cell.getStringCellValue() + " ");
					data.add(cell.getStringCellValue());

				} else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
					//System.out.print(cell.getNumericCellValue() + " ");
					data.add(cell.getNumericCellValue());

				} else if (cell.getCellTypeEnum() == CellType.BOOLEAN) {
					//System.out.print(cell.getBooleanCellValue() + " ");
					data.add(cell.getBooleanCellValue());

				} else { // //Here if require, we can also add below methods to
							// read the cell content
							// XSSFCell.CELL_TYPE_BLANK
							// XSSFCell.CELL_TYPE_FORMULA
							// XSSFCell.CELL_TYPE_ERROR
				}
			}
			System.out.println();
			try {
				XlsxFileToRead.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return data;
	}*/
	
	
	public static List<Comparable> readData(String fileName, String sheetName) {

		List<Comparable> data = new ArrayList<Comparable>();

		FileInputStream XlsxFileToRead = null;
		XSSFWorkbook workbook = null;
		try {

			String filePath = new String(System.getProperty("user.dir") + "\\src\\test\\resources\\datafiles\\smoketest\\" + fileName + ".xlsx");
			//logger.info("File Path is : " + filePath);
			System.out.println(filePath);
			XlsxFileToRead = new FileInputStream(filePath);

			// Getting the workbook instance for xlsx file
			workbook = new XSSFWorkbook(XlsxFileToRead);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// getting the first sheet from the workbook using sheet name.
		// We can also pass the index of the sheet which starts from '0'.
		XSSFSheet sheet = workbook.getSheet(sheetName);
		XSSFRow row;
		XSSFCell cell;

		//Get total rows
		
		
		// Iterating all the rows in the sheet
		Iterator<?> rows = sheet.rowIterator();

		while (rows.hasNext()) {
			row = (XSSFRow) rows.next();
			System.out.println("row Num is ::::: "+sheet.getLastRowNum());

			// Iterating all the cells of the current row
			Iterator<?> cells = row.cellIterator();

			while (cells.hasNext()) {
				cell = (XSSFCell) cells.next();

				if (cell.getCellTypeEnum() == CellType.STRING) {
					//System.out.print(cell.getStringCellValue() + " ");
					data.add(cell.getStringCellValue());

				} else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
					//System.out.print(cell.getNumericCellValue() + " ");
					data.add(cell.getNumericCellValue());

				} else if (cell.getCellTypeEnum() == CellType.BOOLEAN) {
					//System.out.print(cell.getBooleanCellValue() + " ");
					data.add(cell.getBooleanCellValue());

				} else { // //Here if require, we can also add below methods to
							// read the cell content
							// XSSFCell.CELL_TYPE_BLANK
							// XSSFCell.CELL_TYPE_FORMULA
							// XSSFCell.CELL_TYPE_ERROR
				}
			}
			System.out.println();
			try {
				XlsxFileToRead.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	return data;
	}
	




}


