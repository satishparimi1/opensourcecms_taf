package opensourceCMS.com.genericlib;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class SeleniumUtil  extends BaseTest{

	
	
	
	
	public static void waitForPageToLoad() {
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
	
	/*
	 * Actions Class -
	 * 1) Mouse hover opertion
	 * 2) Double Click - contextClick();
	 * 3) Drag and drop
	 * 
	 */
	
	//Mouse hover operation
	public static void mouseHover(WebElement element) {
	
		Actions act = new Actions(driver);
		act.moveToElement(element).perform();
		
	}
	
	
	
	/*
	 * Window Handling
	 */
	
	
	//Window Handling
	public static void swithToSecondWindow() {
		
		//get Window IDs
		Set<String> windowIDs = driver.getWindowHandles();
		
		Iterator<String> itr = windowIDs.iterator();
		
		String parent_Window_ID = itr.next();
		
		String child_Window_ID = itr.next();
		
		driver.switchTo().window(child_Window_ID);
	}
	
	
	//TODO
	public static void swithToThirdWindow() {
		
		//get Window IDs
		Set<String> windowIDs = driver.getWindowHandles();
		
		Iterator<String> itr = windowIDs.iterator();
		
		String parent_Window_ID = itr.next();
		
		String child_Window_ID = itr.next();
		
		driver.switchTo().window(child_Window_ID);
		
		//TODO - Write code here
		
	}
	
	
	//Alert Handling
	public static void acceptAlert() {
		Alert alt = driver.switchTo().alert();
		
		alt.accept();
	}
	
	public static void cancelAlert() {
		Alert alt = driver.switchTo().alert();
		alt.dismiss();
	}
	
	public static void getAlertText() {
		Alert alt = driver.switchTo().alert();
		
		alt.getText();
	}
	
	
	//TODO
	/*
	 * Text-box
	 * 1) Enter the text
	 * 2) Clear the text box
	 * 
	 */
	
	//TODO
	/*
	 * Buttons
	 * 1) Click operation
	 * 
	 * 
	 */
	
	//TODO
	/*
	 * Dropdowns
	 * 1) Click operation
	 * 
	 * 
	 */
	//Select value from drop down
	public static void selectValuebyVisibleText(WebElement Dropdown_Locator, String option) {
		
		Select sel = new Select(Dropdown_Locator);
		sel.deselectByVisibleText(option);
	}
	
	public static void selectValuebyValue(WebElement Dropdown_Locator, String option) {
		
		Select sel = new Select(Dropdown_Locator);
		sel.deselectByValue(option);
	}
	
	
	//TODO
	/*
	 * Checkbox
	 * 1) Click operation
	 * 
	 * 
	 */
	
	
	/*
	 * Links
	 * 
	 * 1) Click
	 * 
	 */
	
	/*
	 * Get current window URL
	 * 
	 * 
	 */
	
	
	//Get current window URL
	public String getCurrentWindowURL() {
		
		String currentURL = driver.getCurrentUrl();
		
		return currentURL;
	}
	
	//Get get Website title
	public String getTitile() {
		
		String websitetitle = driver.getTitle();
		
		return websitetitle;
		
		
	}
}
