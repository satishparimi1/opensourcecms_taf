package opensourceCMS.com.genericlib;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class CustomReporter extends BaseTest implements ITestListener, ISuiteListener  {

	@Override
	public void onStart(ISuite suite) {
		
		htmlReporter=new ExtentHtmlReporter(System.getProperty("user.dir")+"\\ExtentReports\\SmokeTest_Result.html");
		extent= new ExtentReports();
		extent.attachReporter(htmlReporter);
		
		extent.setSystemInfo("OS", System.getProperty("os.name"));
		try {
			extent.setSystemInfo("Host Name", InetAddress.getLocalHost().getHostName());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Reporter.log("Hostname can not be resolved");
		}
		extent.setSystemInfo("Environment Name", "Testging Environment");
		extent.setSystemInfo("User Name", System.getProperty("user.name"));
		
		htmlReporter.config().setChartVisibilityOnOpen(true);
		htmlReporter.config().setDocumentTitle("Amazon Test Results");
		htmlReporter.config().setReportName("Smoke_Test_Suite_Results");
		htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
		htmlReporter.config().setTheme(Theme.STANDARD);
		
	}

	@Override
	public void onFinish(ISuite suite) {
		
		extent.flush();
	}
	
	
	
	

	@Override
	public void onTestStart(ITestResult result) {
		
		test = extent.createTest(result.getName());
		
		
	}

	

	@Override
	public void onTestSuccess(ITestResult result) {
		
		//Adding success test status
		test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case is PASSED : ",ExtentColor.GREEN));

		String screenShotPath = captureScreenshot(driver, result.getName());		
			try {
				test.addScreenCaptureFromPath(screenShotPath);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Reporter.log("Capture screenshot failed", true);
			}
				
	}

	@Override
	public void onTestFailure(ITestResult result) {
		//Adding Failed Test status
		test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test Case FAILED due to below issues: ", ExtentColor.RED));
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		//Adding Skipped Test status
		test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED :",ExtentColor.ORANGE));
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		
		
	}
	
	
	

	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

	
}
